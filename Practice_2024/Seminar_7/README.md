# TODO:
### - Везде.
- Форматирование + нумерация тем и подтем.

### - Функции. Процедуры. Триггеры:
+ solved.sql
### - View:
+ solved.sql

# Семинар 7.

## Overview

> ### Примечание:
> При желании разобраться более подробно - рекомендую к прочтению официальную документацию:<br>
> [postgresql - на русском](https://postgrespro.ru/docs/postgresql/current/)<br>
> [postgresql - docs (en)](https://www.postgresql.org/docs/current/)

## Материалы к семинарам:

## - [Теоретико-множественные операции, примеры](Set/SET.md)

## - [VIEW](View/VIEW.md)

## - [Функции. Процедуры. Триггеры.](Trigger/TRIGGER.md)


