# Семинар 8.

### - [Индексы](Index/INDEX.md)

### - [Партиционирование. Вертикальная и горизонтальная масштабируемость.](SCALABILITY.md)

### - [Горизонтальное масштабирование, хранение на разных машинах](HORIZONTAL_SCALABILITY.md)

### - [OLAP](OLAP.md)

### - [ETL](ETL/README.md)

### - [CHECK OPTION](CHECK_OPTION.md)
