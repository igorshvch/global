-- Оконные функции.

--1. Создать таблицу и заполнить ее данными согласно скрипту.


--2. Вывести все товары и среднюю цену товара в каждой из категорий в отдельном столбце. Ответ округлить до целого

select p.product_id, p.product_name
     , pg.group_name
     , round(avg(p.price) over(partition by p.group_id), 0) as avg_price
  from sem_5.products p
  join sem_5.product_groups pg
    on p.group_id = pg.group_id;

--3. Вывести номер товара в порядке возрастания цены в каждой группе.

 select p.product_id, p.product_name
     , pg.group_name, p.price
     , row_number() over(partition by p.group_id order by p.price asc)
  from sem_5.products p
  join sem_5.product_groups pg
    on p.group_id = pg.group_id;

--4. Повторить предыдущий шаг, но товары с одинаковой ценой должны иметь одинаковый порядковый номер.

 select p.product_id, p.product_name
     , pg.group_name, p.price
     , dense_rank() over(partition by p.group_id order by p.price asc)
     , rank() over(partition by p.group_id order by p.price asc)
  from sem_5.products p
  join sem_5.product_groups pg
    on p.group_id = pg.group_id;
 

--5. Вывести для каждого товара разность его цены с предыдущим в одной товарной категории.

select p.product_id, p.product_name
     , pg.group_name, p.price
     , p.price - lag(p.price, 1) over(partition by p.group_id order by p.price) as price_diff
  from sem_5.products p
  join sem_5.product_groups pg
    on p.group_id = pg.group_id;

--6. Для каждого товара вывести наименьшую стоимость в данной товарной категории.

select p.product_id, p.product_name
     , pg.group_name, p.price
     , max(p.price) over(partition by p.group_id) as max_group_price
  from sem_5.products p
  join sem_5.product_groups pg
    on p.group_id = pg.group_id; 
    
-- CTE

--1. Используя таблицу, созданную в предыдущем задании, вывести самый дорогой товар в каждой из категорий.

with max_price as
(
select group_id, max(price) as max_price
  from sem_5.products
 group by group_id
)
select p.product_id
     , p.product_name
     , p.price
     , p.group_id
  from sem_5.products p
  join max_price mp
    on p.group_id = mp.group_id
   and p.price = mp.max_price;

--2. Используя CTE вывести категории, имеющие более 6 товаров.

with group_counts as
(
select group_id, count(1) as group_cnt
  from sem_5.products
 group by group_id
)
select pg.group_id, pg.group_name
  from sem_5.product_groups pg
  join group_counts gc
    on pg.group_id = gc.group_id
 where gc.group_cnt > 6;
-- Рекурсия

--1. Написать запрос для вывода первых 10 членов геометрической прогресси с первым членом = 1 и множителем = 2.

with recursive geom(val, step) as
(
   values(1, 1)
    union all
   select val * 2, step + 1 from geom
    where step < 10
)

select val, step from geom;

--2. Написать запрос для вывода первых 30 чисел Фибоначи.


with recursive fibonacci(val1, val2, step) as (
    values(1, 1, 1)
      union all
    select val2, val1 + val2, step + 1 from fibonacci
    where step < 30
)

SELECT val1, step FROM fibonacci;

--3. Создать таблицу согласно скрипту.


--4. Вывести для менеджера с employee_id = 2 всех подчиненных с помощью рекурсии.

WITH RECURSIVE emp AS (
  SELECT 
    employee_id, 
    manager_id, 
    full_name 
  FROM 
    employees 
  WHERE 
    employee_id = 2 
  UNION 
  SELECT 
    e.employee_id, 
    e.manager_id, 
    e.full_name 
  FROM 
    employees e 
    INNER JOIN emp s ON s.employee_id = e.manager_id
) 

SELECT * FROM emp;
